Quickly verify log booking (qa,local,dev)

#### Usage

Install all dependencies (see error log) (Use Python2)

./triptest <list of env>

eg:

./triptest qa dev
*****Executing tests for qa environment****** 
creating booking for hotel 
save and get succesful for trip Q1811130355 and product hotel  
creating booking for air 
save and get succesful for trip Q1811130356 and product air 
creating booking for local 
save and get succesful for trip Q1811130357 and product local 
creating booking for train 
save and get succesful for trip Q1811130358 and product train 
*****Executing tests for dev environment****** 
creating booking for hotel 
save and get succesful for trip P1811130001 and product hotel 
creating booking for air 
save and get succesful for trip P1811130002 and product air 
creating booking for local 
save and get succesful for trip P1811130003 and product local 
creating booking for train 
save and get succesful for trip P1811130004 and product train 

(GET is verified as saved based on misc content which should have the term 'trip_service') 


#### TODO

- Prod testing (with dns not ip) to ensure lb is working as expected

#### Make it an executable to run from anywhere in the shell

sudo ln -s /home/sudipbhandari/triptest/triptest  /usr/local/bin/triptest

(modify the paths)
