import requests 
import json

#'failures' contains the list of trip refs (line by line)
f=open('failures','r')

for trip in f.readlines():
	resp=requests.get("http://trip-service.cltp.com:9001/trips?tripID={}".format(trip.strip()))
	r = resp.text
	d = json.loads(r)
	t = d.get('txns')
	for each in t:
		if each.get('txn_type')==1:
			if each.get('status')=='C':
				if 'cron' in each.get('misc'):
					print("Trip with ref {} has been closed with payment drop off scheduler".format(trip))
